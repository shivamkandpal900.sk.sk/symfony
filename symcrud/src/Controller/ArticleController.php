<?php
namespace App\Controller;

use App\Entity\Article;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
//use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class ArticleController extends Controller {
    /**
     * @Route("/", name="article_list")
     * @Method({"GET"})
     */
    public function index() {

        // return new Response('<html> 
        // <body> hello </body></title>');
        
        // $articles = ['Article 1', 'Article 2'];

        $articles  = $this->getDoctrine()->getRepository
        (Article::class)->findAll();
      
     return $this->render('articles/index.html.twig', 
     array('articles' => $articles));
    }

 // create a user
    /**
     * @Route("/article/new", name = "new_article")
     * Method({"GET", "POST"})
     */
    public function new(Request $request) {
        $article = new Article();

        $form  = $this->createFormBuilder($article)
        ->add('firstname', TextType::class, array
        ('attr'=> array('class' => 'form-control')))

        ->add('lastname', TextType::class, array
        ('attr'=> array('class' => 'form-control')))

        ->add('username', TextType::class, array
        ('attr'=> array('class' => 'form-control')))
        
        // 'required' => false,
        // 'attr'=> array('class' => 'form-control')

        ->add('save', SubmitType::class, array(
          'label' => 'Create',
          'attr' => array('class' => 'btn btn-primary mt=3')
        ))
        ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $article = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager -> persist($article);
            $entityManager -> flush();

            return $this->redirectToRoute('article_list');
        }

        return $this->render('articles/new.html.twig', array(
            'form' => $form->createView()
        ));


    } // close  function new

    
 // create a user
    /**
     * @Route("/article/edit/{id}", name = "edit _article")
     * Method({"GET", "POST"})
     */
    public function edit(Request $request, $id) {
        $article = new Article();
        $article = $this->getDoctrine()->getRepository
        (Article::class)->find($id);


        $form  = $this->createFormBuilder($article)
        ->add('firstname', TextType::class, array
        ('attr'=> array('class' => 'form-control')))

        ->add('lastname', TextType::class, array
        ('attr'=> array('class' => 'form-control')))

        ->add('username', TextType::class, array
        ('attr'=> array('class' => 'form-control')))
        
        // 'required' => false,
        // 'attr'=> array('class' => 'form-control')

        ->add('save', SubmitType::class, array(
          'label' => 'Update',
          'attr' => array('class' => 'btn btn-primary mt=3')
        ))
        ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager-> flush();

            return $this->redirectToRoute('article_list');
        }

        return $this->render('articles/edit.html.twig', array(
            'form' => $form->createView()
        ));
    }




    // show a value
    /**
     * @Route("/articles/{id}", name="article_show")
     */
    public function show($id) {
        $article = $this->getDoctrine()->getRepository 
        (Article::class)->find($id);

        return $this->render('articles/show.html.twig', 
        array('article' => $article));
    }

    // delete function

     /**
      * @Route ("/article/delete/{id}")
      * @Method({"DELETE"})
      */
      public function delete(Request $request, $id){
          $article  = $this->getDoctrine()->getRepositore(Article::class)->find($id);

          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->remove($article);
          $entityManager->flush();

          $response = new Response();
          $response->send();
      }
     
     /**
      * @Route("/article/save")
      */
      public function save() {
          $entityManager = $this->getDoctrine()->getManager();

          $article = new Article();
          $article->setFirstname('Vijay');
          $article->setLastname('Rawat');
          $article->setUsername('Vijay');

          $entityManager->persist($article);

          $entityManager->flush();

          return new Response('Saves Sucessfully'.$article->getId());
      }
    
    }  // close ArticleController